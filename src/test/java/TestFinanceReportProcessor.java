import Arrays.FinanceReport;
import Arrays.FinanceReportProcessor;
import Arrays.Payments;
import org.junit.Assert;
import org.junit.Test;

public class TestFinanceReportProcessor {
    @Test
    public void testGetReportsStartsWithSymbol(){
        Payments paymentsArray[] = new Payments[]{
                new Payments("Abc", 2000, 3, 20, 10000),
                new Payments("Bca", 2000, 3, 20, 15000),
                new Payments("Acb", 2000, 3, 20, 25000)
        };

        Payments resultArray[] = new Payments[]{
                new Payments("Abc", 2000, 3, 20, 10000),
                new Payments("Acb", 2000, 3, 20, 25000)
        };

        FinanceReport report = new FinanceReport(paymentsArray, "Report", 2000,  10, 10);
        Assert.assertArrayEquals(resultArray, FinanceReportProcessor.getReportsStartsWithSymbol(report, 'A'));
    }

    @Test
    public void testGetReportsLessThan(){
        Payments paymentsArray[] = new Payments[]{
                new Payments("Abc", 2000, 3, 20, 10000),
                new Payments("Bca", 2000, 3, 20, 15000),
                new Payments("Acb", 2000, 3, 20, 25000)
        };

        Payments resultArray[] = new Payments[]{
                new Payments("Abc", 2000, 3, 20, 10000)
        };

        FinanceReport report = new FinanceReport(paymentsArray, "Report", 2000,  10, 10);
        Assert.assertArrayEquals(resultArray, FinanceReportProcessor.getReportsLessThan(report, 15000));
    }
}
