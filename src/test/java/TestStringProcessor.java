import Strings.StringProcessor;
import org.junit.Assert;
import org.junit.Test;

public class TestStringProcessor {
    @Test
    public void testMultiply(){
        Assert.assertEquals("aaa", StringProcessor.multiply(3, "a"));
        Assert.assertEquals("", StringProcessor.multiply(0, "a"));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testMultiplyException(){
        Assert.assertEquals("aaa", StringProcessor.multiply(-3, "a"));
    }

    @Test
    public void testAmountOfSubstrings(){
        Assert.assertEquals(4, StringProcessor.amountOfSubStrings("abababab", "ab"));
    }

    @Test
    public void testChangeNumbers(){
        Assert.assertEquals("одиндватри", StringProcessor.changeNumbers("123"));
    }

    @Test
    public void  testDeleteEveryEven(){
        Assert.assertEquals("135", StringProcessor.deleteEveryEven("123456"));
    }

}
