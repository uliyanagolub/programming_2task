import Inheritance.BeginStringFilter;
import Inheritance.EndStringFilter;
import Inheritance.SubstringFilter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStringFilter {
    @Test
    public void testBeginString(){
        BeginStringFilter filter1 = new BeginStringFilter("Мама");
        BeginStringFilter filter2 = new BeginStringFilter("мыла");
        String str = "Мама мыла раму";
        assertEquals(true, filter1.apply(str));
        assertEquals(false, filter2.apply(str));
    }
    @Test
    public void testSubstring(){
        SubstringFilter filter1 = new SubstringFilter("Мама");
        SubstringFilter filter2 = new SubstringFilter("кек");
        String str = "Мама мыла раму";
        assertEquals(true, filter1.apply(str));
        assertEquals(false, filter2.apply(str));
    }
    @Test
    public void testEndString(){
        EndStringFilter filter1 = new EndStringFilter("раму");
        EndStringFilter filter2 = new EndStringFilter("Мама");
        String str = "Мама мыла раму";
        assertEquals(true, filter1.apply(str));
        assertEquals(false, filter2.apply(str));
    }
}
