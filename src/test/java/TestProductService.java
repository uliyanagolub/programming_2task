import Inheritance.*;
import org.junit.Assert;
import org.junit.Test;

public class TestProductService {
    @Test
    public void testCountByFilter() {
        BeginStringFilter filter = new BeginStringFilter("штучный");
        ProductService productService = new ProductService();
        Product product1 = new Product("весовой продукт", "весовой товар");
        Packing packing = new Packing("упаковка", 10);
        Packed[] packed = new Packed[]{
                new PackedPieceProduct("штучный товар", "штучный товар", 100, packing, 2),
                new PackedWeightProduct(product1, 10000, packing)
        };
        Consignment consignment = new Consignment("какие-то товары", packed);
        Assert.assertEquals(1, productService.countByFilter(consignment, filter));
    }
}
