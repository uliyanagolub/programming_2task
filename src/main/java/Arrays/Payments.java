package Arrays;

import java.util.Objects;

public class Payments {
    String name;
    int year;
    int month;
    int day;
    int money;

    public Payments(){
        name = "";
    }

    public Payments(String name, int year, int month, int day, int money){
        this.name = name;
        if (year > 1800 & year < 3000){
            this.year = year; }
        else {
            throw new IllegalArgumentException("Неверный год"); }

        if (month > 0 & month < 13){
            this.month = month; }
        else {
            throw new IllegalArgumentException("Неверный месяц"); }
        if (day > 0 & day < 32){
            this.day = day; }
        else {
            throw new IllegalArgumentException("Неверный день"); }
        this.money = money;

    }

    public String getName(){
        return name;
    }

    public int getYear(){
        return year;
    }

    public int getMonth(){
        return month;
    }

    public int getDay(){
        return day;
    }

    public int getMoney() {return money;}

    public void setName(String name) { this.name = name; }

    public void setYear(int year){ this.year = year; }

    public void setMonth(int month) { this.month = month; }

    public void setDay(int day) { this.day = day; }

    public void setMoney(int money) { this.money = money; }

    public void setPayment (Payments other){
        this.name = other.getName();
        this.day = other.getDay();
        this.month = other.getMonth();
        this.year = other.getYear();
        this.money = other.getMoney();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payments payments = (Payments) o;
        return year == payments.year &&
                month == payments.month &&
                day == payments.day &&
                money == payments.money &&
                Objects.equals(name, payments.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, year, month, day, money);
    }

    @Override
    public String toString() {
        return "Arrays.Payments{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", money=" + money +
                '}';
    }

    public void printPayments(){
        System.out.print(name + year + month + day) ;
    }
}

