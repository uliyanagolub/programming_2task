package Arrays;

import java.util.ArrayList;

public class FinanceReportProcessor {
    public static Payments[] getReportsStartsWithSymbol (FinanceReport report, char symbol){
        ArrayList<Payments> filtered = new ArrayList<>();

        for (int i = 0; i < report.paymentsAmount(); i++ ){
            if (report.getPaymentI(i).getName().startsWith(String.valueOf(symbol))){
                filtered.add(report.getPaymentI(i));
            }
        }

        Payments[] filteredPayments = new Payments[filtered.size()];

        for (int i = 0; i < filtered.size(); i++){
            filteredPayments[i] = filtered.get(i);
        }

        return filteredPayments;
    }

    public static Payments[] getReportsLessThan(FinanceReport report, int value) {
        ArrayList<Payments> filteredReports = new ArrayList<>();

        for (int i = 0; i < report.paymentsAmount(); i++ ){
            if (report.getPaymentI(i).getMoney() < value){
                filteredReports.add(report.getPaymentI(i));
            }
        }

        Payments[] filteredPayments = new Payments[filteredReports.size()];

        for (int i = 0; i < filteredReports.size(); i++){
            filteredPayments[i] = filteredReports.get(i);
        }

        return filteredPayments;
    }
}
