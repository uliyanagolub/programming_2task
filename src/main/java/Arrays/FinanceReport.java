package Arrays;

public class FinanceReport {
    private Payments[] paymentsArray;
    private String reporterName;
    private int year, month, day;

    public FinanceReport(int number, String reporterName, int day, int month, int year){
        this.reporterName = reporterName;
        this.day = day;
        this.month = month;
        this.year = year;
        this.paymentsArray = new Payments[number];

    }

    public FinanceReport(Payments[] paymentsArray, String reporterName, int year, int month, int day) {
        this.paymentsArray = paymentsArray;
        this.reporterName = reporterName;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public FinanceReport(FinanceReport another){

        this.reporterName = another.getReporterName();
        this.day = another.getDay();
        this.month = another.getMonth();
        this.year = another.getYear();
        this.paymentsArray = new Payments[ another.paymentsAmount()];
        for (int i = 0; i < another.paymentsAmount(); i++){
            this.paymentsArray[i] = another.getPaymentI(i);
        }

    }

    public String getReporterName() {
        return reporterName;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int paymentsAmount(){
        return paymentsArray.length;
    }

    public Payments getPaymentI (int number){
        return paymentsArray[number];
    }

    public void setPaymentI (int number, Payments newPayment){
        paymentsArray[number] = newPayment;
    }

    public String toString(FinanceReport report){
        StringBuilder string = new StringBuilder();

        string.append(String.format( "[Автор: %s, дата: %d.%d.%d Платежи: [ \n", report.reporterName, report.day,
               report.month, report.year));
        for (Payments payment : report.paymentsArray){
            string.append( String.format("     Плательщик: %s, дата: %d.%d.%d. сумма: %d руб. %d коп.\n",
                   payment.name, payment.day, payment.month, payment.year, payment.money/100, payment.money%100));
        }
        return string.toString();
    }
}
