package Strings;

public class StringProcessor {
    public static String multiply(int n, String string){
        if (n < 0){
            throw new IllegalArgumentException("n < 0");
        }
        if (n == 0){
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder(string);
        for (int i = 0; i < n - 1; i++){
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    public static int amountOfSubStrings(String string1, String string2){
        int count = 0;
        for(int i = 0; i < string1.length(); i++){
            if(string1.substring(i).startsWith(string2)){
                count++;
            }
        }
        return count;
    }

    public static String changeNumbers(String string){
        StringBuilder stringBuilder = new StringBuilder(string);
        for(int i = 0; i < stringBuilder.length(); i ++){
            if (stringBuilder.substring(i).startsWith("1")) {
                stringBuilder.replace(i, i + 1, "один");
            }
            if (stringBuilder.substring(i).startsWith("2")){
                stringBuilder.replace(i, i+1, "два");
            }
            if (stringBuilder.substring( i).startsWith("3")){
                stringBuilder.replace(i, i+1, "три");
            }
        }
        return stringBuilder.toString();
    }

    public static String deleteEveryEven (String string){
        StringBuilder stringBuilder = new StringBuilder(string);
        for ( int i = 1; i < stringBuilder.length(); i++ ){
            stringBuilder.delete(i, i+1);
        }
        return stringBuilder.toString();
    }





}

