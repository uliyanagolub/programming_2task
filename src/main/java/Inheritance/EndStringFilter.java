package Inheritance;

public class EndStringFilter implements Filter{
    private String pattern;

    public EndStringFilter(String pattern) {
        this.pattern = pattern;
    }

    public boolean apply(String str) {
        return str.endsWith(pattern);
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
