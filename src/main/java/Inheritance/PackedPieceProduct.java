package Inheritance;

public class PackedPieceProduct extends PieceProduct implements Packed{
    private Packing packing;
    private int amount;
    private boolean weightProduct = false;

    public PackedPieceProduct(String name, String description, int weight, Packing packing, int amount) {
        super(name, description, weight);
        this.packing = packing;
        this.amount = amount;
    }

    public boolean isPieceProduct() {
        return weightProduct;
    }

    public void setPieceProduct(boolean pieceProduct) {
        this.weightProduct = pieceProduct;
    }

    @Override
    public double getNetMass() {
        return getWeight()*amount;
    }

    @Override
    public double getGrossMass() {
        return getNetMass() + packing.getWeight();
    }

    @Override
    public boolean isSetProduct(int i) {
        return false;
    }

    @Override
    public boolean isWeightProduct() {
        return weightProduct;
    }
}
