package Inheritance;

public class Consignment {
    private String description;

    private Packed[] packed;

    public Consignment(String description, Packed ... packed) {
        this.description = description;
        this.packed = packed;
    }

    double getConsignmentMass(){
        double sum = 0;
        for (int i = 0; i < packed.length; i++){
            sum += packed[i].getGrossMass();
        }
        return sum;
    }

    public Packed getPacked(int i) {
        return packed[i];
    }

    public int getLength(){
        return packed.length;
    }
}
