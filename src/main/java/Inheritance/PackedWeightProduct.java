package Inheritance;

public class PackedWeightProduct extends WeightProduct implements Packed {
    private Packing packing;
    private double weight;
    private boolean weightProduct = true;

    public PackedWeightProduct(Product product, double weight, Packing packing) {
        super(product.getName(), product.getDescription());
        this.packing = packing;
        this.weight = weight;
    }

    @Override
    public double getNetMass() {
        return weight;
    }

    @Override
    public double getGrossMass() {
        return getNetMass() + packing.getWeight();
    }

    @Override
    public boolean isWeightProduct() {
        return weightProduct;
    }

    @Override
    public boolean isSetProduct(int i) {
        return false;
    }

    @Override
    public void setWeightProduct(boolean weightProduct) {
        this.weightProduct = weightProduct;
    }

}
