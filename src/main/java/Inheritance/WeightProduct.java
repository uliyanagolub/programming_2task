package Inheritance;

public class WeightProduct extends Product {
    private boolean weightProduct = true;

    WeightProduct(String name, String description){
        super(name, description);
    }

    public boolean isWeightProduct() {
        return weightProduct;
    }

    public void setWeightProduct(boolean weightProduct) {
        this.weightProduct = weightProduct;
    }

    @Override
    public String toString() {
        return "WeightProduct " + super.toString();
    }
}
