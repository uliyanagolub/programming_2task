package Inheritance;

import java.util.Objects;

public class Packing {
    private String name;
    private double weight;

    public Packing(String name, double weight){
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Packing)) return false;
        Packing packing = (Packing) o;
        return getWeight() == packing.getWeight() &&
                getName().equals(packing.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getWeight());
    }

    @Override
    public String toString() {
        return "Packing{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
