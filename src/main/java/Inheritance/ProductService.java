package Inheritance;

public class ProductService {

    public static int countByFilter(Consignment consignment, Filter filter) {
        int amount = 0;
        for (int i = 0; i < consignment.getLength(); i++) {
            if (filter.apply(consignment.getPacked(i).getName())){
                amount++;
            }
        }
        return amount;
    }
}
