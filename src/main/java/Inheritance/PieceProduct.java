package Inheritance;

public class PieceProduct extends Product {
    private double weight;
    private boolean pieceProduct = true;
    private boolean weightProduct = false;

    PieceProduct(String name, String description, double weight){
        super(name,description);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "PieceProduct{" +
                "weight=" + weight +
                "} " + super.toString();
    }

    public boolean isPieceProduct() {
        return pieceProduct;
    }

    public void setPieceProduct(boolean pieceProduct) {
        this.pieceProduct = pieceProduct;
    }
}
