package Inheritance;

public class SubstringFilter implements Filter {
    private String substring;

    public SubstringFilter(String substring) {
        this.substring = substring;
    }

    public boolean apply(String str) {
        int index = -1;
        return str.indexOf(substring) > index;
    }

    public String getSubstring() {
        return substring;
    }

    public void setSubstring(String substring) {
        this.substring = substring;
    }
}
