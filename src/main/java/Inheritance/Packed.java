package Inheritance;

public interface Packed {
    double getNetMass();

    double getGrossMass();

    String getName();

    boolean isWeightProduct();

    boolean isSetProduct(int i);
}
